# Aplikasi Config Server #

1. Setting encrypt key di environment variable

    * Windows : `set ENCRYPT_KEY=abcd-abcd-wxyz-wxyz`
    * *Nix : `export ENCRYPT_KEY=abcd-abcd-wxyz-wxyz`

2. Cek apakah encrypt key sudah terpasang atau belum

    * Windows : `echo %ENCRYPT_KEY%`
    * *Nix : `echo $ENCRYPT_KEY`

3. Jalankan aplikasi

        mvn spring-boot:run

4. Encrypt nilai `rahasia` dengan config-server

    * Dengan menggunakan Postman
    
    ![Postman Encrypt](docs/postman-1.png)

    * Dengan menggunakan command line `curl` 
    
        ```
        curl localhost:8080/encrypt -d rahasia
        0eaec9fb0de2bda3ef16a3d9cf81a77b91ad6ac82818bf4a0619df02dbb6c593
        ```

5. Pasang nilai tersebut di file konfigurasi, misalnya seperti ini

        spring.datasource.password={cipher}0eaec9fb0de2bda3ef16a3d9cf81a77b91ad6ac82818bf4a0619df02dbb6c593

6. Pada waktu diakses, yang ditampilkan adalah nilai yang sudah di-_decrypt_

    ![Postman Decrypt](docs/postman-2.png)

## Deployment ke Heroku ##

1. Buat aplikasi di [dashboard Heroku](https://dashboard.heroku.com/apps/)

    ![Heroku Create App](docs/heroku-1.png)

2. Masuk ke tab `Settings`, isi environment variabel berikut:

    * `SPRING_PROFILES_ACTIVE` : `heroku`
    * `ENCRYPT_KEY` : `abcd-abcd-wxyz-wxyz`
    
    ![Heroku Settings](docs/heroku-2.png)

3. Tambahkan tujuan git remote ke heroku, sesuai nilai `Heroku Git URL`

        git remote add heroku https://git.heroku.com/tms-config-server.git

4. Push ke heroku agar terdeploy

        git push heroku master

5. Untuk melihat log aplikasi, install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli). Kemudian jalankan perintah logs

        heroku logs --tail